import React from "react";
import styles from "../styles/Loader.module.css";
function Loader() {
  return (
    <div className={styles.loader}>
      <p className="Loader">Loading...</p>
    </div>
  );
}

export default Loader;
